############
Description
############

************
Introduction
************

This repository contains a docker-compose to create a Nextcloud server. The stack uses Postgres as database, Nginx as webserver.

A separated Nginx container is used to deal with SSL certificates and reserve proxy.

This configuration uses HTTPS, and the certificates for the HTTPS are in a volume container. You may read `these steps <https://gitlab.com/raill/lets-encrypt-certificate-from-container>`_ for generating a letsencrypt certificate with container, and adjust the current docker-compose file to fit your scenario.

Insert your config settings
===========================

Do not forget to change:

* the **POSTGRES_PASSWORD** variable due to security issues.
* the **cloud.example.com** address in both nginx.conf file (webserver and reverse proxy) to your domain.
  

*******
Install
*******

After create the network and run the containers, the service will be available for the administrative configuration through an web browser.

Network
========


Create a local bridge network.

.. code-block:: bash

       docker network create --driver bridge reverse-proxy

Check if the network was correctly created.

.. code-block:: bash

       docker network ls

       NETWORK ID          NAME                DRIVER              SCOPE
       f63535b753f2        bridge              bridge              local
       90cda90cb9a4        host                host                local
       59f18da77331        none                null                local
       a262a4c95f58        reverse-proxy       bridge              local

To see the network details.

.. code-block:: bash

       docker network inspect reverse-proxy

Containers
==========

To start the containers, run the commands bellow from the same folder of the docker-compose files, in the ``cloud`` and ``reverse`` directory, respectively. 

.. code-block:: bash

   cd cloud/
   docker-compose up -d
   cd ..

   cd reverse/
   docker-compose up -d
   cd ..

******************
Update the service
******************

Do not Skip Major releases
==========================

The `Nextcloud documention <https://docs.nextcloud.com/server/latest/admin_manual/maintenance/upgrade.html>`_ recommends **do not** skipping major releases. So, if you intend to install the 19 release from the 17 one, for instance, then you must install the 18 version as a intermediate process.

    *"It is best to keep your Nextcloud server upgraded regularly, and to install all point releases and major releases. Major releases are 11, 12, and 13. Point releases are intermediate releases for each major release. For example, 13.0.4 and 12.0.9 are point releases. Skipping major releases is not supported."*

Enable the maintenance mode
===========================

Set the `maintenance mode <https://docs.nextcloud.com/server/stable/admin_manual/configuration_server/occ_command.html?highlight=maintenance%20mode#maintenance-commands-label>`_ in the Nextcloud container before update the containers. 

Check if the ``cloud_nextcloud_1`` is actual the name of the container running Nextcloud. Run ``docker ps -a`` to verify.

.. code-block:: bash

    docker exec -u www-data -it cloud_nextcloud_1 sh

    php occ maintenance:mode --on
    
Press down the ``<CTRL>`` while press the ``p`` and then the ``q`` keys to leave the container without stopping. 

Remove the old containers
=========================

Remember to **Keep the volumes**.

.. code-block:: bash

   cd cloud/

   docker-compose down

   cd ..

Start the service
=================

.. code-block:: bash

   cd cloud/
   
   docker-compose up -d --build

   cd ..
  
Disable the maintenance mode
============================

.. code-block:: bash

    docker exec -u www-data -it cloud_nextcloud_1 sh

    php occ maintenance:mode --off

Then continue the configurations in the site admin area.
    
******************
Remove the service
******************

To remove the service, delete the containers, volumes, and networks.

Networks
========

The internal networks are removed automatically.

Run the follow command to remove the external network.

.. code-block:: bash

       docker network rm reverse-proxy

Containers
==========

From the same folder of the docker-compose file.

.. code-block:: bash

   cd cloud/ 

   docker-compose stop

   docker-compose rm

Volumes
=======

The **volumes** keep data after restarting services or deleting containers to, for instance, install the Nextcloud's releases updates.

**Be sure before remove them**. Backup any useful data from the volumes. The data will be **completely lost** after deleting the volumes.

Fist list the volumes that will be deleted.

.. code-block:: bash

  docker volume ls -q | grep cloud_

Then, if **only** the volumes you want to delete were listed, proceed:

.. code-block:: bash

  docker volume rm $(docker volume ls -q | grep cloud_)
